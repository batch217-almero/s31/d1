// REQUIRE DIRECTORY-Load a particular module node.js
// Also called  Node.js dependencies

// http is a node module/application
// http is composed of different components
let http = require("http");//Hyper Text Transfer Protocol

let port = 3000;//declare port variable

http.createServer(function (request, response){
	// writeHead- code 200: OK, No Problem, Success
	response.writeHead(200, {"Content-Type" : "text/plain"});
	response.end("Hello World!");
}).listen(port);

// A port is a virtual point where network connections start and end
// Each port is associated with specific process or service(dapat isang port isang type of service lang)

console.log(`Server is running at localhost: ${port}`);