const http = require("http");

const port = 4000;

// Store create server in a variable (using arrow function)
const server = http.createServer((request, response) =>{
	// 1st endpoint is /greeting, display the ff:
	if (request.url =="/greeting"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Hello Again!");//display in browser
	} 
	// 2nd endpoint is /homepage, display the ff:
	else if (request.url =="/homepage"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("This is the homepage!");//display in browser
	}
	// if wala sa endpoints list yung url, then display the ff
	else {
		response.writeHead(404, {"Content-Type" : "text/plain"});
		response.end("ERROR: 404 - PAGE NOT FOUND")		
	}
});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}.`)